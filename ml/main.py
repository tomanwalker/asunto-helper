
## dep
import json
import pandas as pd
from datetime import date, timedelta 

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR

## config
FILE_DUMP = './data/dump.json'
FILE_DIST = './data/dist-list.json'
FILE_RESULT = './data/test.json'

LABEL = 'price_m'
COLUMNS = ['year', 'size_num', 'route_min', 'residents', 'angle']
RES_MAP = {
    'Omakotitalo': 1,
    'Paritalo': 2,
    'Rivitalo': 5,
    'Kerrostalo': 20,
    'Erillistalo': 1,
    'Luhtitalo': 2
}
COMPASS = {
    'n': 0,
    'ne': 45,
    'e': 90,
    'se': 135,
    's': 180,
    'sw': 225,
    'w': 270,
    'nw': 315
}

## fund
def read_json(fileName):
    f = open(fileName)
    data = json.load(f)
    f.close()
    return data

def single_att(arr, att):
    return list(map(lambda x: x[att], arr))
    
def split_scale(X, Y, TEST_SPLIT=0.25):
	X_train,X_test,y_train,y_test=train_test_split(X,Y,test_size=TEST_SPLIT,shuffle=False)
	
	# normalization
	scaler = StandardScaler()
	# Don't cheat - fit only on training data
	scaler.fit(X_train)
	X_train = scaler.transform(X_train)
	X_test = scaler.transform(X_test)
	
	return X_train,X_test,y_train,y_test, scaler
#enddef

def getLinearModel(name='mlp'):
	
	if name == 'svr':
		return SVR(epsilon=0.2)
	
	return MLPRegressor(random_state=1, max_iter=500)
#enddef

## flow
data = read_json(FILE_DUMP)
dist = read_json(FILE_DIST)

## add features 
# residents count = type of house
# direction = west (270) / north 0
for x in data:
    houseType = x['kind']
    x['residents'] = RES_MAP[houseType]
    
    ##search(dist, 'name'
    
    x['angle'] = 0
    dist_found = list(filter( lambda d: x['dist'].lower() == d['name'].lower(), dist ))
    if len(dist_found) > 0:
        x['angle'] = COMPASS[dist_found[0]['rel']]
    
#end-for
#print(data[0])

df = pd.json_normalize(data)
Y = df[LABEL]
X = df[COLUMNS]
#print(X)
#print(Y)

X_train,X_test,y_train,y_test,scaler = split_scale(X, Y)
model = getLinearModel('svr')
model.fit(X_train, y_train)
score = model.score(X_test, y_test)
print( score )

# select latest
today = date.today()
limit =  today - timedelta(days=30)
latest = df[
    (df['ts'] >= str(limit)) & (df['residents'] < 10 )
    & (df['year'] >= 1980)
].copy()
print( latest[['ts', 'price']].head(5) )

# scale & forecast
X_latest = scaler.transform(latest[COLUMNS])
pred = model.predict(X_latest)
latest['pred'] = round(pred * latest['size_num'] / 1000, 0) * 1000

## find max Value
latest['diff'] = latest['pred'] -  latest['price_num'];
#latest = latest[(latest['diff'] > 0)]
latest.sort_values(by=['diff'], inplace=True, ascending=False)
print( latest[['pred', 'price_num', 'diff']].head(5) )

## save
json_str = latest.to_json(orient='records')
parsed = json.loads(json_str)
with open(FILE_RESULT, 'w') as json_file:
    json_file.write(json.dumps(parsed, indent=4 ))






