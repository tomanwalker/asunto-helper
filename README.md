# Asunto-helper
Web scrapper and statistics to check around housing
Developed for Espoo in mind, can be applied elsewhere at need.

## Reqs
```

$ node -v
v16.14.1

$ python --version
Python 3.10.6

```

## Run
```
npm install

REAL=1 node runner.js 	## gets remote data
python ml/main.py 			## runs Linear regression
node report.js 					## generates HTML report

```

##

