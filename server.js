
//## dependency
const express = require('express');

const view = require('./lib/view');

const config = require('./config');

//## config
const server = express();

//## func

//## routes
server.get('/', function(req, res){
	return res.redirect('/main');
});
server.get('/main', function(req, res){
	var merged = view.render('main', res.locals);
	return res.send(merged);
});
server.get('/report', function(req, res){
	res.send('actual HTML');
});

// public files
var filesPath = __dirname + '/html';
console.log('filesPath = %s', filesPath);
server.use('/', express.static(filesPath));

//## flow
server.listen(config.PORT, function(){
	console.log('main - server - started = %s', config.PORT);
});


