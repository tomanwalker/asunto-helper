
// ## dep
var dba = require('./lib/dba');

// ## config
var fileData = './data/dump.json';
var fileGood = './data/good.json';
var fileValue = './data/test.json';
var fileResult = './data/report.html';
var fileReportTemplate = './views/report_template.html';

var profileStub = {
	own: 18000,
	first: true
};

// ## export
var ns = {};
module.exports = ns;

// ## fund
ns.combine = function(good, value){
	
	good.forEach(function(x){
		var match = value.find(y => y.id === x.id);
		if( match ){
			x.pred = match.pred;
			x.diff = match.diff;
		}
	});
	
	return good;
};

ns.htmlWrap = function(tag, txt){
	return `<${tag}>${txt}</${tag}>`;
};
ns.htmlAnchor = function(txt, href, newTab){
	var target = (newTab) ? "target=\"_blank\"" : "";
	return `<a href="${href}" ${target}>${txt}</a>`;
};

ns.view = function(data){
	
	console.log(JSON.stringify(data[0], null, 2));
	var cols = ['dist', 'kind', 'price_num', 'size_num', 'rooms', 'year', 'ts', 'url', 
		'pr', 'route_side', 'route_min', 'pred', 'diff', 'offer'];
	
	var mapped = data.map(function(x){
		
		var pr_target = (x.price_m  / x.pr) * 16 * x.size_num;
		x.offer = (Math.round(Math.min(pr_target, x.price_num, x.pred) * 0.9 / 1000)) + 'k';
		
		var rowList = [];
		cols.forEach(function(r){
			
			var cellData = x[r] || "";
			
			if( r === 'ts' ){
				cellData = cellData.slice(0,10);
			}
			if( r === 'url' ){
				cellData = ns.htmlAnchor(cellData, cellData, true);
			}
			
			rowList.push( cellData );
		});
		
		return ns.htmlWrap("tr", rowList.map(h => ns.htmlWrap("td", h)).join("") );
	});
	
	var txt = "<table class=\"table table-striped\">\n";
	txt += ns.htmlWrap("tr", cols.map(h => ns.htmlWrap("th", h)).join("") ) + "\n";
	txt += mapped.join("\n");
	txt += "\n</table>";
	
	return txt;
};

ns.main = function(){
	var dataGood = dba.readJSON(fileGood);
	var dataValue = dba.readJSON(fileValue);
	
	var dataCombined = ns.combine(dataGood, dataValue);
	var table = ns.view(dataCombined);
	
	var template = dba.readFile( fileReportTemplate );
	var txt = template.replace("{{body}}", table);
	dba.writeFile(fileResult, txt);
};

// ## flow
if( require.main === module ){
	ns.main(process.argv.slice(2));
}


