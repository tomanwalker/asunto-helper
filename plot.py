
## dep
import matplotlib.pyplot as plt
import json

## config
DATA_FILE = './data/dump.json'

## func
def read_json(fileName):
    f = open(fileName)
    data = json.load(f)
    f.close()
    return data
#end-def

def single_att(arr, att):
    return list(map(lambda x: x[att], arr))
#end-def

def render(arr):
    fig = plt.figure()
    
    y = single_att(arr, 'price_m')
    x1 = single_att(arr, 'year')
    x2 = single_att(arr, 'route_min')
    x3 = single_att(arr, 'size_num')
    x4 = single_att(arr, 'pr')
    
    plt.subplot(2, 2, 1)
    plt.plot(x1, y, 'o', color='r')
    plt.xlabel("year")
    
    plt.subplot(2, 2, 2)
    plt.plot(x2, y, 'o', color='green')
    plt.xlabel("route")
    
    plt.subplot(2, 2, 3)
    plt.plot(x3, y, 'o', color='blue')
    plt.xlabel("size")
    
    plt.subplot(2, 2, 4)
    plt.plot(y, x4, 'o', color='m')
    
    min_price = min(y)
    max_price = max(y)
    dash_x = [min_price, max_price]
    plt.plot(dash_x, [16, 16], linestyle='dashed', color='gray')
    plt.plot(dash_x, [20, 20], linestyle='dashed', color='gray')
    plt.ylabel("price-to-rent")
    
    plt.tight_layout()
    plt.show()
    
#end-def

## flow
data = read_json(DATA_FILE)

render(data)


