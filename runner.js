/*
* Alex Shkunov, 2022
*/

// ## dep
var md5 = require("crypto-js/md5");

var dba = require('./lib/dba');
var utils = require('./lib/utils');
var stats = require('./lib/stats');
var etl = require('./lib/etl');

var config = require('./config');

// ## config
var dbFile = './data/dump.json';
var fileGood = './data/good.json';
var fileRent = './data/price-to-rent.json';
var fileDist = './data/dist-list.json';
var fileReview = './data/review.json';

var profileId = "123";

// ## export
var ns = {};
module.exports = ns;

// ## fund
ns.matchArea = function(x){
	
	var x_low = x.toLowerCase();
	
	return function(d){
		var d_low = d.name.toLowerCase();
		return x_low.includes( d_low );
	};
};
ns.goodFilter = function(now){
	
	now = new Date();
	var now_year = now.getFullYear();
	var age_limit = 40;
	
	return function(x){
		var age_diff = now_year - x.year;
		return (
			age_diff <= age_limit
			&& x.route_min < 30
			&& (
				['w', 'sw', 's', 'se', 'e'].includes( x.route_side ) // prefer south side
				|| (
					// if north - should be closer and newer
					x.route_min < 20
					&& age_diff <= (age_limit - 10)
					&& x.kind !== "Kerrostalo"
				)
			)
			&& x.size_num >= 72
		);
	};
};
ns.goodSort = function(a, b){
	
	var a_pr = Math.round(a.pr / 2) * 2;
	var b_pr = Math.round(b.pr / 2) * 2;
	
	if( a_pr < b_pr){
		return -1;
	}
	if( a_pr > b_pr ){
		return 1;
	}
	
	var a_r = Math.round(a.route_min / 1) * 1;
	var b_r = Math.round(b.route_min / 1) * 1;
	if( a_r < b_r ){
		return 1;
	}
	if( a_r > b_r ){
		return -1;
	}
	
	var a_m = Math.round(a.price_m / 10 ) * 10;
	var b_m = Math.round(b.price_m / 10 ) * 10;
	if( a_r < b_r ){
		return 1;
	}
	if( a_r > b_r ){
		return -1;
	}
	
	return 0;
};

ns.main = async function(...args){
	
	console.log( 'main - start...');
	
	var dist_known = await etl.run();
	
	var dist_list = dba.readJSON(fileDist);
	var pr_num = stats.getRentAvg(fileRent);
	
	dist_known.forEach(function(x){
		
		var dist_obj = dist_list.find(ns.matchArea(x.dist.toLowerCase()) );
		
		x.pr = utils.roundTo(x.price_m / pr_num, 2);
		x.route_min = dist_obj.min;
		x.route_price = utils.roundTo( x.price_num / x.route_min, 2);
		x.route_side = dist_obj.rel;
		
		x.id = md5(x.city + x.dist + x.street + x.price + x.year + x.size).toString();
	});
	
	// save new & remove old
	await dba.updateCatalog(dbFile, dist_known);
	
	// skip already not-suitable deals
	var dataReview = dba.readJSON(fileReview);
	var profileReview = dataReview.rows.find( x => x.profile === profileId );
	var currentDeals = dist_known.filter(function(x){
		var reviewTarget = profileReview.targets.find(t => t.id === x.id);
		var shouldSkip = (reviewTarget || {}).hide;
		
		return !shouldSkip;
	});
	
	// find good deals
	var good_deals = currentDeals
		.filter( ns.goodFilter() )
		.sort(ns.goodSort)
		.slice(0, config.GOOD_LEN);
	
	console.log( ' Good deals = %j ', good_deals.map(x => Math.round(x.pr) ) );
	console.log( '======' );
	await dba.writeJSON(fileGood, good_deals);
	
	// Stats (avg price-to-rent)
	var stat_obj = stats.getFinalStats(dist_known);
	stat_obj.avg_rent_m = utils.roundTo( pr_num / 12, 2);
	
	console.log( ' Stats = %j ', stat_obj );
	console.log( '======' );
	
	return 0;
};

// ## flow
if( require.main === module ){
	ns.main(process.argv.slice(2));
}


