
// ##
// ##
// ## export
var ns = {};
module.exports = ns;

// ## fund
ns.roundTo = function(num, places){
	return +(Math.round(num + "e+" + places)  + "e-" + places);
};
ns.reduceSum = function(a,b){
	return a+b;
};
ns.partialTextMatch = function(a, b){
	
	var a_low = a.toLowerCase();
	var b_low = b.toLowerCase();
	return a_low.includes(b_low);
	
};


