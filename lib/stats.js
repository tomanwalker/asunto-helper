
// ## dep
var utils = require('./utils');
var dba = require('./dba');

// ##
// ## export
var ns = {};
module.exports = ns;

// ## fund
ns.getFinalStats = function(arr){
	var stats = {
		len: arr.length
	};
	
	if( stats.len === 0 ){
		return stats;
	}
	
	var total_pr_sum = arr.map(x => x.pr).reduce(utils.reduceSum);
	var total_size_sum = arr.map(x => x.size_num).reduce(utils.reduceSum);
	var total_price_sum = arr.map(x => x.price_num).reduce(utils.reduceSum);
	
	stats.price_to_rent_avg = utils.roundTo(total_pr_sum / stats.len, 2);
	stats.size_avg = utils.roundTo(total_size_sum / stats.len, 2);
	stats.price_avg = utils.roundTo(total_price_sum / stats.len, 2);
	
	return stats;
};

ns.getRentAvg = function(fileRent){
	var dict = dba.readJSON( fileRent );
	var date = new Date();
	
	var sum = dict.rows.map(function(x){
		var d = new Date(x.date);
		var diff = date - d;
		if( diff > (90 * 24 * 60 * 60 * 1000) ){
			console.log('Alert - old rent = %s', x.date);
		}
		
		return (x.rent / x.sq);
	}).reduce(utils.reduceSum);
	
	var avg = utils.roundTo(utils.roundTo(sum / dict.rows.length, 2) * 12, 2);
	console.log( 'stats.getRentAvg - end - Avg rent = %s', avg);
	console.log( '======' );
	return avg;
};


