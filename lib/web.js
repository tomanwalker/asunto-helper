
// ## dep
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');
const fs = require('fs');
//const { clickCmp } = require("puppeteer-cmp-clicker");

var config = require('../config');

// ## config

// ## exp
var ns = {};
module.exports = ns;

// ## fund
ns.sleep = function(ms){
	return new Promise(function(resolve, reject){
		setTimeout(resolve, ms);
	});
};

ns.getWeb = async function(url){
	
	console.log('getWeb - limit = %s | url = %s', config.LIMIT_PAGES, url);
	
	const browser = await puppeteer.launch({headless: false});
	const page = await browser.newPage();
	var contents = [];
	
	for(var i=1; i<= config.LIMIT_PAGES; i++){
		var target = url + '&pagination=' + i;
		await page.goto(target);
		await ns.sleep(100);
		/*
		const cookFrame = page.frames().find(f => f.url().startsWith('https://cmp.oikotie.fi/index.html'));
		var cook = await cookFrame.$('button');
		console.log(cook);
		await clickCmp({ page: page, customKnownClasses: ['message-component', 'message-button'] });
		return 0;
		*/
		const html = await page.content();
		fs.writeFileSync(`./screens/test-${i}.html`, html);
		
		contents.push(html);
		//await page.screenshot({path: 'example.png'});
		
	}
	
	await browser.close();
	return contents;
	
};

ns.extract = async function(data){
	
	var contents = ( typeof(data) === 'string' ) ? [data] : data;
	var parsed = [];
	var d = new Date();
	var ts = d.toISOString();
	
	for( var html of contents ){
		const j = await cheerio.load(html);
		//console.log( 'main - cheerio - j = %j', Object.keys(j) );
		var cards = j(".cards card");
		
		var arr = cards.map(function(idx, el){
			
			var obj = {};
			
			obj.street = j(".ot-card__street", el).text(); 
			obj.city = j('[ng-if=$ctrl.card.building.city]', el).text();
			obj.price = j('.ot-card__price', el).text();
			obj.size = j('.ot-card__size', el).text();
			
			obj.rooms = j('[ng-bind=$ctrl.card.roomConfiguration]', el).text();
			/*
				4h,k,s ja autotalli
				4h+k+s+parveke+autokatospaikka, yj:n mukaan 4h+k+s 
				3h,k,s + autokatos
			*/
			obj.kind = j('[ng-if=$ctrl.card.subType]', el).text();
			var year = j('[ng-bind=$ctrl.card.building.year]', el).text();
			obj.year = Number( year );
			obj.dist = j('[ng-bind=$ctrl.card.building.district]', el).text();
			
			obj.car = (
				obj.rooms.includes('autotalli')
				||
				obj.rooms.includes('autokatospaikka')
				||
				obj.rooms.includes('autokatos')
			);
			obj.size_num = Number( obj.size.replace(' m²', '').replace(',','.') );
			obj.price_num = Number( obj.price.replace(' €', '').replace(' ', '') );
			obj.price_m = Math.round( obj.price_num / obj.size_num );
			obj.ts = ts;
			obj.url = j('[analytics-click="search_result_click"]', el).attr('href');
			
			return obj;
		});
		
		parsed = parsed.concat(arr.toArray());
	}
	
	return parsed;
};


