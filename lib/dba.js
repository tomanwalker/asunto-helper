
// ## dep
var fs = require('fs');

var utils = require('./utils');

// ## config
var DEL_TIME = (120 * 24 * 60 * 60 * 1000); // 120 days

// ## export
var ns = {};
module.exports = ns;

// ## fund
ns.readFile = function(fileName){
	return fs.readFileSync(fileName, 'utf-8');
};
ns.writeFile = function(fileName, txt){
	return fs.writeFileSync(fileName, txt);
};
ns.readJSON = function(fileName){
	var txt = ns.readFile(fileName);
	return JSON.parse(txt);
};
ns.writeJSON = function(fileName, body){
	var txt = JSON.stringify(body, null, 2);
	return ns.writeFile(fileName, txt);
};
ns.readFileMany = function(dir){
	var files = fs.readdirSync(dir).filter(x => x.includes('.html') );
	
	var arr = files.map(function(x){
		return ns.readFile(dir + '/' + x);
	});
	
	return arr;
};

ns.updateCatalog = async function(dbFile, freshList){
	// save new
	var saved = [];
	var dir = dbFile.split('/').slice(0, -1).join('/');
	
	if( !fs.existsSync(dir) ){
		console.log('Creating dir = %s', dir);
		fs.mkdirSync(dir);
	}
	
	try{
		saved = await ns.readJSON(dbFile);
	}
	catch(err){
		
	}
	
	// remove old
	var len_init = saved.length;
	var date = new Date();
	saved = saved.filter(function(el){
		var ts = new Date(el.ts);
		return (date - ts) < DEL_TIME;
	});
	var cnt_removed = len_init - saved.length;
	
	// save new
	var len_before = saved.length;
	freshList.forEach(function(x){
		var exist = saved.find( z => x.id === z.id );
		if( !exist ){
			saved.push(x);
		}
	});
	var cnt_insert = saved.length - len_before;
	console.log( 'Inserted = %s | removed = %s', cnt_insert, cnt_removed);
	console.log( '======' );
	
	if( cnt_insert !== 0 || cnt_removed !== 0){
		await ns.writeJSON(dbFile, saved);
	}
};


