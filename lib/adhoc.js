
var dba = require('./dba');

var data = dba.readJSON('./data/dist-map.json');
console.log('Keys = %s', Object.keys(data).length );

var list = [];
for(var a in data){
	var obj = Object.assign({name: a}, data[a]);
	list.push(obj);
}

list.sort(function(a,b){
	
	if( a.min < b.min ){
		return -1;
	}
	if( a.min > b.min ){
		return 1;
	}
	if( a.km < b.km ){
		return -1;
	}
	if( a.km > b.km ){
		return 1;
	}
	
	return 0;
});

dba.writeJSON('./data/dist-list.json', list);


