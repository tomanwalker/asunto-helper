
// ## dep
var dba = require('./dba');
var web = require('./web');
var utils = require('./utils');

var config = require('../config');

// ## config
var fileDist = './data/dist-list.json';

var sortBy = 'published_sort_desc'; //'price_asc'; //'price_desc' // published_sort_desc
var query = '?cardType=100'
	//+ '&buildingType[]=1&buildingType[]=256' // kerros
	+ '&buildingType[]=2' // rivi
	+ '&buildingType[]=64' // pari
	+ '&buildingType[]=4&buildingType[]=8&buildingType[]=32&buildingType[]=128' //omakoti
	+ '&roomCount[]=3&roomCount[]=4'
	+ `&price[min]=${config.PRICE_MIN}&price[max]=${config.PRICE_MAX}`
	+ `&locations=["${config.CITY}"]&sortBy=${sortBy}`;
var target_url = config.BASE_URL + query;

// ## export
var ns = {};
module.exports = ns;

// ## fund
ns.extract = async function(){
	var contents = ( config.REAL ) ? await web.getWeb(target_url) : dba.readFileMany('./screens');
	var arr = await web.extract(contents);
	return arr;
};

ns.run = async function(){
	
	console.log( 'elt.run - start - real = %s', config.REAL);
	
	var arr = await ns.extract();
	console.log( 'Extracted = %s', arr.length );
	
	var dist_list = dba.readJSON(fileDist);
	var dist_new = arr.filter(function(x){
		
		return !dist_list.find(function(d){
			return utils.partialTextMatch(x.dist, d.name);
		});
		
	}).sort();
	console.log( '** map missing **' );
	console.log(dist_new.map(x => x.dist));
	console.log( '======' );
	
	var dist_known = arr.filter(function(x){
		return dist_list.find(function(d){
			return (
				utils.partialTextMatch(x.dist, d.name)
				&& x.year > 1900
			);
		});
	});
	
	return dist_known;
};


