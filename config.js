

module.exports = {
	PORT: Number(process.env.PORT || 9000),
	
	BASE_URL: "https://asunnot.oikotie.fi/myytavat-asunnot",
	
	REAL: (process.env.REAL === '1'),
	CITY: process.env.CITY || "Espoo",
	PRICE_MIN: process.env.PRICE_MIN || (150 * 1000),
	PRICE_MAX: process.env.PRICE_MAX || (360 * 1000),
	
	LIMIT_PAGES: Number(process.env.LIMIT_PAGES || 5),
	GOOD_LEN: Number(process.env.GOOD_LEN || 10)
};


